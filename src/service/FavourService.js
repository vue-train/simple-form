import Vue from 'vue';
import axios from 'axios';

const Api = (headers) => {
  const defaultHeaders = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };

  return axios.create({
    baseURL: '/',
    withCredentials: true,
    // mode: 'no-cors',
    credentials: true,
    headers: { ...defaultHeaders, ...headers },
  });
};

export default new Vue({
  computed: {
    api() {
      return Api();
    },
  },

  methods: {
    appealList(params) {
      return this.api.get('saveUrl', params);
    },
  },
});
