const ServiceTypes = {
  MOBILE: 'MOBILE',
  HOME: 'HOME',
  SATELLITE: 'SATELLITE',
};
export default ServiceTypes;
